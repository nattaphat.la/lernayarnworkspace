"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

// import client from './api-client'
var client;
var localStorageKey;

function handleUserResponse(_ref) {
  var token = _ref.token,
      user = _objectWithoutProperties(_ref, ["token"]);

  window.localStorage.setItem(localStorageKey, token);
  return user;
}

function getUser() {
  var token = getToken();

  if (!token) {
    return Promise.resolve(null);
  }

  return client.getUser()["catch"](function (error) {
    logout();
    return Promise.reject(error);
  });
}

function login(_ref2) {
  var username = _ref2.username,
      password = _ref2.password;
  return client.login({
    username: username,
    password: password
  }).then(handleUserResponse);
}

function register(_ref3) {
  var username = _ref3.username,
      password = _ref3.password;
  return client.register({
    username: username,
    password: password
  }).then(handleUserResponse);
}

function logout() {
  window.localStorage.removeItem(localStorageKey);
  return Promise.resolve();
}

function getToken() {
  return window.localStorage.getItem(localStorageKey);
}

var _default = function _default(_client) {
  var _localStorageKey = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'token';

  localStorageKey = _localStorageKey;
  client = _client(_localStorageKey);
  return {
    login: login,
    register: register,
    logout: logout,
    getToken: getToken,
    getUser: getUser
  };
};

exports["default"] = _default;
//# sourceMappingURL=auth-client.js.map