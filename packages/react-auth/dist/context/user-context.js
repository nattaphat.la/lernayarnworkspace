"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.UserProvider = UserProvider;
exports.useUser = useUser;

var _react = _interopRequireDefault(require("react"));

var _authContext = require("./auth-context");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var UserContext = _react["default"].createContext();

function UserProvider(props) {
  var _useAuth = (0, _authContext.useAuth)(),
      user = _useAuth.data;

  return _react["default"].createElement(UserContext.Provider, _extends({
    value: user
  }, props));
}

function useUser() {
  var context = _react["default"].useContext(UserContext);

  if (context === undefined) {
    throw new Error("useUser must be used within a UserProvider");
  }

  return context;
}
//# sourceMappingURL=user-context.js.map