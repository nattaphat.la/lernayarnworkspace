"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _context = require("./context");

Object.keys(_context).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _context[key];
    }
  });
});

var _authContext = require("./context/auth-context");

Object.keys(_authContext).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _authContext[key];
    }
  });
});

var _userContext = require("./context/user-context");

Object.keys(_userContext).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _userContext[key];
    }
  });
});
//# sourceMappingURL=index.js.map