// import client from './api-client'
let client
let localStorageKey

function handleUserResponse({token, ...user}) {
  window.localStorage.setItem(localStorageKey, token)
  return user
}

function getUser() {
  const token = getToken()
  if (!token) {
    return Promise.resolve(null)
  }
  return client.getUser().catch(error => {
    logout()
    return Promise.reject(error)
  })
}

function login({username, password}) {
  return client.login({username, password}).then(handleUserResponse)
}

function register({username, password}) {
  return client.register({username, password}).then(handleUserResponse)
}

function logout() {
  window.localStorage.removeItem(localStorageKey)
  return Promise.resolve()
}

function getToken() {
  return window.localStorage.getItem(localStorageKey)
}

export default (_client, _localStorageKey = 'token') => {
  localStorageKey = _localStorageKey
  client = _client(_localStorageKey)
  return {login, register, logout, getToken, getUser}
}
