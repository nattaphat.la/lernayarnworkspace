import { jsx } from '@emotion/core';
import React from 'react'
import {useAsync} from 'react-async'
import client from '../utils/auth-client'

const AuthContext = React.createContext()

function AuthProvider(props) {
  const {
    authClient: _authClient,
    spinnerComponent: SpinnerComponent,
    getUserFailComponent: GetUserFailComponent,
    ...rest
  } = props;
  const authClient = client(_authClient)
  const [firstAttemptFinished, setFirstAttemptFinished] = React.useState(false)
  const {
    data = null,
    error,
    isRejected,
    isPending,
    isSettled,
    reload,
  } = useAsync({
    promiseFn: authClient.getUser,
  })

  React.useLayoutEffect(() => {
    if (isSettled) {
      setFirstAttemptFinished(true)
    }
  }, [isSettled]) // call if isSettled change
  if (!firstAttemptFinished) {
    if (isPending) {
      return <SpinnerComponent />
    }
    if (isRejected) {
      return <GetUserFailComponent />
    }
  }

  const login = form => authClient.login(form).then(reload)
  const register = form => authClient.register(form).then(reload)
  const logout = () => authClient.logout().then(reload)

  return (
    <AuthContext.Provider value={{data, login, logout, register}} {...rest} />
  )
}

function useAuth() {
  const context = React.useContext(AuthContext)
  if (context === undefined) {
    throw new Error(`useAuth must be used within a AuthProvider`)
  }
  return context
}

export {AuthProvider, useAuth}
