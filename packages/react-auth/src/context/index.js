import React from 'react'
import {AuthProvider} from './auth-context'
import {UserProvider} from './user-context'

function AppProviders({children, ...rest}) {
  return (
    <AuthProvider {...rest}>
      <UserProvider>{children}</UserProvider>
    </AuthProvider>
  )
}

export {
  AppProviders
}
