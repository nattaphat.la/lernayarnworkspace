"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

// import client from './api-client'
var client;
var localStorageKey;

function handleUserResponse(_ref) {
  var accessToken = _ref.accessToken,
      user = _objectWithoutProperties(_ref, ["accessToken"]);

  window.localStorage.setItem(localStorageKey, token);
  return user;
}

function getUser() {
  var token = getToken();

  if (!token) {
    return Promise.resolve(null);
  }

  return client.getUser()["catch"](function (error) {
    logout();
    return Promise.reject(error);
  });
}

function login(_ref2) {
  var username = _ref2.username,
      password = _ref2.password;
  return client.login({
    username: username,
    password: password
  }).then(handleUserResponse);
}

function register(_ref3) {
  var username = _ref3.username,
      password = _ref3.password;
  return client.register({
    username: username,
    password: password
  }).then(handleUserResponse);
}

function logout() {
  window.localStorage.removeItem(localStorageKey);
  return Promise.resolve();
}

function getToken() {
  return window.localStorage.getItem(localStorageKey);
}

var _default = authClient = function authClient(_client) {
  var _localStorageKey = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'token';

  localStorageKey = _localStorageKey;
  client = _client(_localStorageKey);
  return {
    login: login,
    register: register,
    logout: logout,
    getToken: getToken,
    getUser: getUser
  };
};

exports["default"] = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AuthProvider = AuthProvider;
exports.useAuth = useAuth;

var _react = _interopRequireDefault(require("react"));

var _reactAsync = require("react-async");

var _authClient2 = _interopRequireDefault(require("./auth-client"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var AuthContext = _react["default"].createContext();

function AuthProvider(props) {
  var _authClient = props.authClient,
      spinnerComponent = props.spinnerComponent,
      getUserFailComponent = props.getUserFailComponent,
      rest = _objectWithoutProperties(props, ["authClient", "spinnerComponent", "getUserFailComponent"]);

  var authClient = (0, _authClient2["default"])(_authClient);

  var _React$useState = _react["default"].useState(false),
      _React$useState2 = _slicedToArray(_React$useState, 2),
      firstAttemptFinished = _React$useState2[0],
      setFirstAttemptFinished = _React$useState2[1];

  var _useAsync = (0, _reactAsync.useAsync)({
    promiseFn: authClient.getUser
  }),
      _useAsync$data = _useAsync.data,
      data = _useAsync$data === void 0 ? null : _useAsync$data,
      error = _useAsync.error,
      isRejected = _useAsync.isRejected,
      isPending = _useAsync.isPending,
      isSettled = _useAsync.isSettled,
      reload = _useAsync.reload;

  _react["default"].useLayoutEffect(function () {
    if (isSettled) {
      setFirstAttemptFinished(true);
    }
  }, [isSettled]); // call if isSettled change


  if (!firstAttemptFinished) {
    if (isPending) {
      return jsx(SpinnerComponent, null);
    }

    if (isRejected) {
      return jsx(GetUserFailComponent, null);
    }
  }

  var login = function login(form) {
    return authClient.login(form).then(reload);
  };

  var register = function register(form) {
    return authClient.register(form).then(reload);
  };

  var logout = function logout() {
    return authClient.logout().then(reload);
  };

  return jsx(AuthContext.Provider, _extends({
    value: {
      data: data,
      login: login,
      logout: logout,
      register: register
    }
  }, rest));
}

function useAuth() {
  var context = _react["default"].useContext(AuthContext);

  if (context === undefined) {
    throw new Error("useAuth must be used within a AuthProvider");
  }

  return context;
}
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _authContext = require("./context/auth-context");

Object.keys(_authContext).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _authContext[key];
    }
  });
});
