
import Axios from 'axios';

let client = Axios.create()

function getUser() {
  return client.get('/users/me').then(res => res.data)
}
function login(payload) {
  return client.post('/users/login', payload).then(res => res.data).then(({id, userId, ...user}) => {
    return {
      token: id,
      id: userId,
      ...user
    }
  })
}
function register() {
  return client.post('/users', payload).then(res => res.data)
}
export default (API_URL) => (localStorageKey) => {
  const token = window.localStorage.getItem(localStorageKey)
  client = Axios.create({
    baseURL: API_URL,
    headers: {
      Authorization: token
    }
  })
  return {
    getUser,
    login,
    register,
    client
  }
}